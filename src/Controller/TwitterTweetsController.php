<?php

namespace Drupal\twitter_tweets\Controller;
use Drupal\Core\Controller\ControllerBase;

 /** 
  * In case of no default template of module, below markup will be displayed
  */
class TwitterTweetsController extends ControllerBase {
	
  /**
   * A Function to display default Content
   */
	
  public function content() {
    return ['#markup' => 'Twitter Tweets Default!'];
  }
  
}
